Base template to add a Linux CentOS 8/RockyOS 8/Fedora host to a Zabbix Server
This is intended to be used with VMware VM provision, so variables have reference to VMware stuff.
You can replace them with the ones you preffer.

## Variables
- zabbix_server
- ZABBIX_USER (used as ENV var)
- ZABBIX_USER (used as ENV var)
- vsphere_vm_hostname
- vsphere_datacenter
- vsphere_vm_ip_address
- vsphere_vm_hostname
- vsphere_vm_fqdn
- zabbix_proxy (if not necessary, set to '')